#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <ctime>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    main_textBrowser();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    Tea t;
    Coffee c;
    srand(time(NULL));
    int level = rand() % 1000;
    if(ui->radioButton->isChecked()) {
        if(c.Start(level)) {
            QMessageBox::information(this, "Success", "Please, wait about 1 minute, while drink will be ready.");
            ui->textBrowser->setText("Price of Your drink 2$");
        }
        else {
            QMessageBox::critical(this, "Technical Error", "Sorry, low level of water! Add more water.");
        }
    }
    else if(ui->radioButton_2->isChecked()){
        if(t.Start(level)) {
            QMessageBox::information(this, "Success", "Please, wait about 1 minute, while drink will be ready.");
            ui->textBrowser->setText("Price of Your drink 1$");
        }
        else {
            QMessageBox::critical(this, "Technical Error", "Sorry, low level of water! Add more water.");
        }
    }
    else {
        QMessageBox::warning(this, "Warning", "No selected drink!");
    }
}

void MainWindow::main_textBrowser()
{
    ui->textBrowser->setText("Choice drink to make:");
}
