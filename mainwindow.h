#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void main_textBrowser();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

class CoffeeMachine {
public:
    bool Start(int LevelWater) {
        if(CheckWater(LevelWater)) {
            return true;
        }
        else {
            return false;
        }
    }
private:
    bool CheckWater(int LevelWater) {
        if(LevelWater > 100) {
            return true;
        }
        else {
            return false;
        }
    }
};

class Tea : public CoffeeMachine {
public:
    bool Start(int LevelWater) {
        CoffeeMachine cm;
        if(cm.Start(LevelWater)) {
            return true;
        }
        else {
            return false;
        }
    }
};

class Coffee : public CoffeeMachine {
public:
    bool Start(int LevelWater) {
        CoffeeMachine cm;
        if(cm.Start(LevelWater)) {
            return true;
        }
        else {
            return false;
        }
    }
};

#endif // MAINWINDOW_H
